Add (OER) metadata to URLs

https://www.youtube.com/watch?v=TWqh9MtT4Bg

Status: Prototype, no warranty

Problem to solve:

- YouTube has no option for license CC0 or CC BY-ShareAlike
- some website creators have no time / resources for adding machine readable license information to their pages
- add license to google drive documents or other URLs

=> these resources can't be filtered by license by OER creators/educators looking for specific contents (e.g. "I want to create an open biology course, licensed CC0")

Quickest Approach (only needs human power :))

- store schema.org in json
- generate static .html sites and urls
- generate sitemap.xml for google & co
-

Goals:
- minimalistic, no own server
- should work out of the box with Github Pages
- should be findable with Googles Advanced search filter for Creative Commons (see: Screencast)

Placeholder for MoodleNet, quick solution for universities?

1. Fork it
2. Add metadata
3. Add live url to Google Search Index
4. Wait
5. Test

More elaborate approach: Wordpress example
