//= require_tree .

$(function(){
  $("#search-with-google").click(function(e){
    e.preventDefault();
    var url = "https://www.google.com/search?q=site%3Aregistry.oerhoernchen.de+"+
    encodeURI($("#search-term").val());

    // copied from https://github.com/programmieraffe/oerhoernchen
    var license_filter_val = $('select[name="license-filter"]:first').val();
    var url_license_filter = '';
    switch (license_filter_val) {
      case 'only-oer':
        // as_rights=(cc_publicdomain|cc_attribute|cc_sharealike).-(cc_noncommercial|cc_nonderived)
        url_license_filter = '&as_rights=%28cc_publicdomain%7Ccc_attribute%7Ccc_sharealike%29.-%28cc_noncommercial%7Ccc_nonderived%29';
        break;
      case 'nc':
        // as_rights=(cc_publicdomain|cc_attribute|cc_sharealike|cc_noncommercial).-(cc_nonderived)
        url_license_filter = '&as_rights=%28cc_publicdomain%7Ccc_attribute%7Ccc_sharealike%7Ccc_noncommercial%29.-%28cc_nonderived%29';
        break;
      case 'nc-nd':
        // as_rights=(cc_publicdomain|cc_attribute|cc_sharealike|cc_noncommercial|cc_nonderived)
        url_license_filter = '&as_rights=%28cc_publicdomain%7Ccc_attribute%7Ccc_sharealike%7Ccc_noncommercial%7Ccc_nonderived%29';
        break;
    }
    // add filter to url (not possibly by operator in search query)
    url += url_license_filter;

    window.open(url);
  });
});
