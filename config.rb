###
# Compass
###

# Change Compass configuration
# compass_config do |config|
#   config.output_style = :compact
# end

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
# page "/path/to/file.html", :layout => false
#
# With alternative layout
# page "/path/to/file.html", :layout => :otherlayout
#
# A path which all have the same layout
# with_layout :admin do
#   page "/admin/*"
# end

# Proxy pages (https://middlemanapp.com/advanced/dynamic_pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", :locals => {
#  :which_fake_page => "Rendering a fake page with a local variable" }

###
# Helpers
###

# Automatic image dimensions on image_tag helper
# activate :automatic_image_sizes

# Reload the browser automatically whenever files change
# configure :development do
#   activate :livereload
# end

# Methods defined in the helpers block are available in templates
# helpers do
#   def some_helper
#     "Helping"
#   end
# end

set :css_dir, 'stylesheets'

set :js_dir, 'javascripts'

set :images_dir, 'images'


# BY MA: Generate HTML pages from JSON via middleman data files
# important: use after_configuration
after_configuration do

  # this won't work
  # puts data.rwth_civil_engineering.publisher.inspect

  # this works
  # 2DO: Is this really good practice? (access via @app)
  # https://github.com/middleman/middleman/issues/1810#issuecomment-270089273
  # puts @app.data.rwth_civil_engineering.publisher.inspect

  #puts @app.data.@local_data.inspect
  # Assumes the file source/about/template.html.erb exists

  data.publishers.each do |data_file_name,data_file_values|
    # data.publishers.each gives an array (["rwth_civil_engineering",PARSED JSON VALUES)
    # puts local_data.inspect

    # 2DO: check if .items exists
    data_file_values.items.each do |item|
      # we use data file name data_file_values.publisher.name (easier to find)
      proxy "/oer/#{data_file_name.parameterize}/#{item.name.parameterize}.html", "/templates/oer_detail_page.html",
      #vars only set for templates?
      # in layouts, this will be accessible via current_page.metadata[:locals][:meta_title]
      # https://til.hashrocket.com/posts/2c7c22b200-access-middleman-template-vars-from-a-helper
      locals: {
        current_item: item,
        #set publisher data
        current_publisher: data_file_values.publisher,
        # set meta tags
        meta_title: "OER: #{item.name} - #{data_file_values.publisher.name}",
        meta_description: item.description
      },
      ignore: true
    end
  end

  data.single_entries.each do |data_file_name,data_file_values|
    # data.publishers.each gives an array (["rwth_civil_engineering",PARSED JSON VALUES)
    # puts local_data.inspect

    # 2DO: check if .items exists
    data_file_values.items.each do |item|
      # we use data file name data_file_values.publisher.name (easier to find)
      proxy "/oer/#{data_file_name.parameterize}/#{item.name.parameterize}.html", "/templates/oer_detail_page.html",
      #vars only set for templates?
      locals: {
        current_item: item,
        meta_title: "OER: #{item.name}",
        meta_description: item.description
      },
      ignore: true
    end
  end

  data.elasticsearch_exports.each do |data_file_name,data_file_values|

    # 2DO: check if .items exists
    data_file_values.each do |item|
      # we use data file name data_file_values.publisher.name (easier to find)
      proxy "/oer/#{data_file_name.parameterize}/#{item.name.parameterize}.html", "/templates/oer_detail_page_elasticsearch_mapping.html",
      #vars only set for templates?
      locals: {
        current_item: item,
        meta_title: "OER: #{item.name}",
        meta_description: item.description,
        allowed_schemaorg_keys: [:name,:license,:@type,:creator,:publisher,:image,:url,:dateCreated]
      },
      ignore: true
    end
  end

end

# BY MA: add various file, e.g. just for one url
#data.various.each do |item|
#  proxy "/oer/various/#{item.name.parameterize}.html", "/templates/oer_detail_page.html",
  #vars only set for templates?
#  locals: { current_item: item, publisher:false},
#  ignore: true
#end


# Sitemap plugin
# https://github.com/statonjr/middleman-sitemap
activate :sitemap, :hostname => "https://registry.oerhoernchen.de"

# Disable layouts, because it is easier to set head tags
# set :layout, false

# Build-specific configuration
configure :build do
  set :build_dir, 'public'
  set :base_url, "/oerhoernchen-simple-metadata-registry-middleman" # baseurl for GitLab Pages (project name) - leave empty if you're building a user/group website
  activate :relative_assets # Use relative URLs
  # For example, change the Compass output style for deployment
  # activate :minify_css

  # Minify Javascript on build
  # activate :minify_javascript

  # Enable cache buster
  # activate :asset_hash

  # Use relative URLs
  # activate :relative_assets

  # Or use a different image path
  # set :http_prefix, "/Content/images/"
end
