# Simple Open Metadata Storage for Open Educational Resources (OER)

Status: proof-of-concept, edu-hack

Store metadata for URLs in .json-files and publish them as simple HTML pages to the open web, using schema.org-standard and a static site generator.

## 2DOs

⭕️ Provide better example metadata (e.g. for H5P content, e.g. educational sector, e.g. subjects, etc.) - consult [OER metadata group](https://wiki.dnb.de/display/DINIAGKIM/OER-Metadaten-Gruppe)

⭕️ Consult [Steffen Rörtgen oerhoernchen20-docker(https://github.com/sroertgen/oerhoernchen20_docker/tree/sitemap_dev)] to include this metadata in his projects

## Demo

- Live: [http://registry.oerhoernchen.de/](http://registry.oerhoernchen.de/)
- Gitlab-Preview: [https://programmieraffe.gitlab.io/oerhoernchen-simple-metadata-registry-middleman/](https://programmieraffe.gitlab.io/oerhoernchen-simple-metadata-registry-middleman/)

## Description

Background: Metadata discussions are annoying, but maybe important (yet to prove). First challenges quickly arise when educators or institutions are motivated to provide (better) metadata, but services like YouTube won't let them store educational metadata (or even let them choose between creative commons licenses).
Where should this metadata be stored? Databases and repositories are boring, most of the time not very open and institutions struggle to get the human power needed to launch or maintain them.

Why not publish metadata with Open Web technologies? Why not just store these few lines in JSON and put it out there to the world?

This proof-of-concept does the following:

1. metadata storage in `data/`-folder
2. for every OER metadata stored there, static site generator middleman generates a simple HTML page, see: [Example](https://registry.oerhoernchen.de/oer/rwth-civil-engineering/elva-the-railway-signalling-lab.html)
3. This HTML page for an OER contains a machine readable license , which is picked up by Google (and others) `rel="license" href="https://creativecommons.org/licenses/by/4.0/deed.en`
4. The page also contains the schema.org metadata (see source code), which can be also picked up by search engines or OER-related services
5. The page also displays the information to web users

Disclaimer - schema.org is great, but there is one challenge: There are only predefinied fields (such as "learningResourceType"), but you can add any value you like. In Germany the [OER metadata group](https://wiki.dnb.de/display/DINIAGKIM/OER-Metadaten-Gruppe) is working on standards for that.


## Background: problems in educational practices

- YouTube has no option for license CC0 or CC BY-ShareAlike (or other metadata such as "educational sector = higher education")
- OERs are sometimes google drive documents or contents on services, which (like YouTube) do not support educational metadata
- self-hosted: some website creators have no time / resources for adding machine readable license information to their pages

=> These resources can't be filtered by license by creators/educators looking for specific contents (e.g. "I want to create an open biology course, licensed CC0").

## Add metadata for an URL

You can submit a pull request to add or edit metadata in `data/`-folder.

## Open questions

- How to identify OER with schema.org? Which metata value should be stored to indicate clearly, that this is an open learning resource? See:[https://twitter.com/m_andrasch/status/1194000783475433477]()

## For developers

- [Middleman static site generator (MIT license)](https://middlemanapp.com/)
- [Test tool for schema.org data](https://search.google.com/structured-data/testing-tool?hl=de)

### Fork it

Disclaimer: You need to make some adjustments regarding URLs, some things do not work by now because of hardcoded URLs in javascript

1. Fork it
2. Add metadata to `data/`
3. Check out CI/CD pipeline
3. Add live url to Google Search Index [Google Search Console](https://search.google.com/search-console/about)
4. Wait, indexing takes some time
5. Test it

For easier testing regarding licenses in Google, use browser plugin:

[https://github.com/programmieraffe/oerhoernchen-browser-extension](https://github.com/programmieraffe/oerhoernchen-browser-extension)

### Test locally (OSX)
- Preqs: https://middlemanapp.com/basics/install/
- `gem install middleman`
- `bundle install`
- local preview,run: `bundle exec middleman server`
- build: `bundle exec middleman build` (resulting files stored in public/)

(`bundle exec middleman server --instrument` for more information)
